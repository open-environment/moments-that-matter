Summary: Co-creation of a database/library with government that articulates how decisions are made and collaboratively audits the decisions making process.

What’s the idea?
This wraps around and combines with one of the workshops we’re conceptualizing (#2). Moments that matter points to where decisions are made, evaluates the data used for making decisions-- points to where and what third sector data can be more reflective in these processes.

Moments that matter, recognizes decision making moments -- and couples these with new streams of contextual data. We work with federal government to create this list, make it public and then have people figure out (via workshop #2) how to build this concept out further, dissecting how decisions are made, by whom and then auditing decisions. 

